import { ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastCtrl: ToastController) { }

  async mostrarToast(mensagem: string, duracao: number = 2000) {
    const toast = await this.toastCtrl.create({
      message: mensagem,
      duration: duracao,
    });
    toast.present();
  }

}
