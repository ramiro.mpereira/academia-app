import { Injectable } from '@angular/core';
import { ToastService } from './../toast/toast.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  logado: boolean;

  constructor(private toastSvc: ToastService) { }

  fazerLogin(login: string, senha: string) {

    if (login?.toUpperCase() != "RAMIRO") {
      this.toastSvc.mostrarToast("Login inválido!", 5000)
      return false
    }

    if (senha?.toUpperCase() != "123") {
      this.toastSvc.mostrarToast("Senha inválida!", 5000)
      return false
    }

    this.logado = true
    return true

  }
}
