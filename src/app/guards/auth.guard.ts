import { LoginService } from './../services/login/login.service';
import { ToastService } from './../services/toast/toast.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private toastSvc: ToastService,
    private router: Router,
    private loginSvc: LoginService) {

  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (this.loginSvc.logado)
      return true
      
    this.toastSvc.mostrarToast("Acesso não permitido!")
    this.router.navigate([''])
    return false;
  }

}
