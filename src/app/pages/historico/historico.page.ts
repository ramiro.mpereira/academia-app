import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.page.html',
  styleUrls: ['./historico.page.scss'],
})
export class HistoricoPage implements OnInit {

  checkins: Checkin[] = [
    {
      dh_acesso: '15/05/2021 08:00:01',
      sn_acessou: true,
    },
    {
      dh_acesso: '15/05/2021 10:00:02',
      sn_acessou: false,
      obs: 'Fora da localização da academia!'
    },
    {
      dh_acesso: '15/05/2021 10:30:03',
      sn_acessou: false,
      obs: 'Fora do horário agendado!'
    },
  ]

  constructor() { }

  ngOnInit() {
  }

}

interface Checkin {
  dh_acesso: string,
  sn_acessou: boolean,
  obs?: string
}
