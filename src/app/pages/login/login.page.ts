import { LoginService } from './../../services/login/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  login: string
  senha: string

  constructor(private router: Router, private loginSvc: LoginService) { }

  ngOnInit() {
  }

  fazerLogin() {

    const login: boolean = this.loginSvc.fazerLogin(this.login, this.senha)
    if (login) {
      this.router.navigate(['/home'])
    }

  }
}
