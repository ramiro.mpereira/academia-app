import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { ToastService } from './../../services/toast/toast.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  checkinSucesso: boolean = true


  constructor(private alertCtrl: AlertController,
    private geolocation: Geolocation,
    private toastSvc: ToastService) { }

  async fazerCheckIn() {

    let msg = this.checkinSucesso ? "Check-in realizado com sucesso!" : "Fora do horário agendado!"
    if (!(await this.buscarLocalizacao()))
      msg = "Fora da localização da academia!"
    const alert = await this.alertCtrl.create({
      header: "Check-in",
      message: msg,
      buttons: ['OK']
    })

    alert.present()

    this.checkinSucesso = !this.checkinSucesso
  }


  private async buscarLocalizacao() {

    try {
      const posicaoAtual = await this.geolocation.getCurrentPosition({ timeout: 5000 })
      if (posicaoAtual.coords.latitude.toFixed(2) != (-28.64).toFixed(2) || posicaoAtual.coords.longitude.toFixed(2) != (-53.60).toFixed(2))
        throw new Error("Você não está na localização da academia!");
      if (!posicaoAtual)
        throw new Error("Verifique se a permissão de localização foi concedida!");
      return true
    } catch (error) {
      let mensagem = error.message
      if (mensagem == "User denied Geolocation")
        mensagem = "Você negou a permissão de localização!"
      this.toastSvc.mostrarToast(mensagem);
      console.log(error)
      return false
    }
  }


}
