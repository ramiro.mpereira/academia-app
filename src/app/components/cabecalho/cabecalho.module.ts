import { IonicModule } from '@ionic/angular';
import { CabecalhoComponent } from './cabecalho.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [CabecalhoComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [CabecalhoComponent]
})
export class CabecalhoModule { }
