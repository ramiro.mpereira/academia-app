Caso puxe o fonte do git, rodar npm i antes do ionic serve

Comandos:

Para executar o servidor: ionic serve
Para criar novos itens: ionic generate


Dicas:

Interpolation
Operador ternário
Injeção de dependencias
Singletons -> Padrão de Projeto (Design Pattern)
Orientação a Objetos: Paradigma de programação
Const, var, let
Promises e Async/Await (e thenable se quiser)
Camel Case
Observables (subscribe)
Angular forms: Template Driven e o Reactive Forms
Two-way data binding
Angular e Ionic são Single Page Application - SPA



Falta: 

fazer comunicação com a API para ter dados reais
Deploy em produção (construir o app)